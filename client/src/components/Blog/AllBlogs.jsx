import axios from "axios";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const AllBlogs = () => {
  let [blogs, setBlogs] = useState([]);
  let navigate = useNavigate();
  const fetchBlogs = async () => {
    try {
      let response = await axios({
        url: "https://crud-blog-n2n1.onrender.com/blogs",
        method: "GET",
      });
      setBlogs(response.data.data);
    } catch (error) {
      console.log(error.message);
    }
  };
  useEffect(() => {
    fetchBlogs();
  }, []);

  let handleView = (blog) => {
    return () => {
      navigate(`/blogs/${blog._id}`);
    };
  };
  let handleEdit = (blog) => {
    return () => {
      navigate(`/blogs/update/${blog._id}`);
    };
  };
  // const handleDelete = (blog) => {
  //   return async () => {
  //     try {
  //       let response = await axios({
  //         url: `https://crud-blog-n2n1.onrender.com/blogs/${blog._id}`,
  //         method: "DELETE",
  //       });
  //       toast.success(response.data.message);
  //       fetchBlogs();
  //     } catch (error) {
  //       console.log(error.message);
  //     }
  //   };
  // };

  const handleDelete = (blog) => {
    return async () => {
      const confirmed = window.confirm(
        "Are you sure you want to delete this blog?"
      );

      if (confirmed) {
        try {
          let response = await axios({
            url: `https://crud-blog-n2n1.onrender.com/blogs/${blog._id}`,
            method: "DELETE",
          });
          toast.success(response.data.message);
          fetchBlogs();
        } catch (error) {
          console.log(error.message);
        }
      }
    };
  };
  return (
    <>
      <ToastContainer></ToastContainer>
      <div>
        {blogs.map((blog) => {
          return (
            <div
              key={blog._id}
              style={{
                border: "1px solid orange",
                padding: "10px",
                margin: "10px",
              }}
            >
              <h3>Title: {blog.title}</h3>
              <p>Content: {blog.content}</p>
              <p>Tag: {blog.tag}</p>
              <p>Featured: {blog.featured ? "Yes" : "No"}</p>
              <p>Created Date: {blog.createdAt}</p>
              <img src={blog.blogImage} alt="blogImage" height="150px" />
              <div>
                <button onClick={handleView(blog)} className="buttons">
                  View Blog Details
                </button>
                <button onClick={handleEdit(blog)} className="buttons">
                  Edit Blog
                </button>
                <button
                  onClick={handleDelete(blog)}
                  className="buttons btn-delete"
                >
                  Delete Blog
                </button>
              </div>
            </div>
          );
        })}
      </div>
    </>
  );
};

export default AllBlogs;
