import axios from "axios";
import { useEffect, useState } from "react";
import { useCallback } from "react";
import { useDropzone } from "react-dropzone";
import { useNavigate, useParams } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const UpdateBlog = () => {
  let navigate = useNavigate();
  const [title, setTitle] = useState("");
  const [content, setContent] = useState("");
  const [tag, setTag] = useState("");
  const [featured, setFeatured] = useState("");
  const [blogImage, setBlogImage] = useState("");
  let params = useParams();
  let blogId = params.blogId;

  const onDrop = useCallback(async (acceptedFiles) => {
    let formData = new FormData();
    formData.append("file", acceptedFiles[0]);

    try {
      let response = await axios({
        url: "https://crud-blog-n2n1.onrender.com/files/single",
        method: "POST",
        data: formData,
      });
      toast.success(response.data.message);
      setBlogImage(response.data.result);
    } catch (error) {
      console.log(error.message);
    }
  }, []);
  const { getRootProps, getInputProps, isDragActive } = useDropzone({ onDrop });

  useEffect(() => {
    let fetchSingleBlog = async () => {
      try {
        let response = await axios({
          url: `https://crud-blog-n2n1.onrender.com/blogs/${blogId}`,
          method: "GET",
        });
        setTitle(response.data.result.title);
        setContent(response.data.result.content);
        setTag(response.data.result.tag);
        setFeatured(response.data.result.featured);
        setBlogImage(response.data.result.blogImage);
      } catch (error) {
        console.log(error.message);
      }
    };
    fetchSingleBlog();
  }, [blogId]);

  let handleSubmit = async (e) => {
    e.preventDefault();

    let data = {
      title: title,
      content: content,
      tag: tag,
      featured: featured,
      blogImage: blogImage,
    };

    try {
      let response = await axios({
        url: `https://crud-blog-n2n1.onrender.com/blogs/${blogId}`,
        method: "PATCH",
        data: data,
      });
      toast.success(response.data.message);
      setTimeout(() => {
        navigate("/blogs");
      }, 1000);
    } catch (error) {
      console.log(error.message);
    }
  };
  return (
    <>
      <ToastContainer></ToastContainer>
      <div>
        <h3>Blog Update Form:</h3>
        <form
          onSubmit={handleSubmit}
          style={{ border: "1px dotted black", padding: "25px" }}
        >
          <div>
            <label htmlFor="title">Blog Title:</label>
            <input
              type="text"
              name="title"
              id="title"
              value={title}
              onChange={(e) => {
                setTitle(e.target.value);
              }}
            />
          </div>
          <br />
          <div>
            <label htmlFor="content">Blog Description:</label>
            <textarea
              name="content"
              id="content"
              rows="5"
              value={content}
              onChange={(e) => {
                setContent(e.target.value);
              }}
            />
          </div>
          <br />
          <div>
            <label htmlFor="tag">Tag:</label>
            <input
              type="text"
              name="tag"
              id="tag"
              value={tag}
              onChange={(e) => {
                setTag(e.target.value);
              }}
            />
          </div>
          <br />
          <div>
            <input
              type="checkbox"
              name="featured"
              id="featured"
              checked={featured}
              onChange={(e) => {
                setFeatured(e.target.checked);
              }}
            />
            <label htmlFor="featured">Featured?</label>
          </div>
          <br />
          <div
            {...getRootProps()}
            style={{
              border: "1px dotted black",
              width: "50%",
              padding: "10px",
            }}
          >
            <input {...getInputProps()} />
            {isDragActive ? (
              <p>Drop the files here ...</p>
            ) : (
              <p>Drag and drop some files here, or click to select files</p>
            )}
            {blogImage ? (
              <img src={blogImage} alt="blogImage" height="150px" />
            ) : null}
          </div>
          <br />
          <button type="submit">Update Blog</button>
        </form>
      </div>
    </>
  );
};

export default UpdateBlog;
