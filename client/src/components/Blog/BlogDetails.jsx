import axios from "axios";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

const BlogDetails = () => {
  let [blog, setBlog] = useState({});
  let params = useParams();
  let blogId = params.blogId;

  useEffect(() => {
    let fetchSingleBlog = async () => {
      try {
        let response = await axios({
          url: `https://crud-blog-n2n1.onrender.com/blogs/${blogId}`,
          method: "GET",
        });
        setBlog(response.data.result);
      } catch (error) {
        console.log(error.message);
      }
    };
    fetchSingleBlog();
  }, [blogId]);
  return (
    <>
      <div>
        <h3>Title: {blog.title}</h3>
        <p>Content: {blog.content}</p>
        <p>Tag: {blog.tag}</p>
        <p>Featured: {blog.featured ? "Yes" : "No"}</p>
        <p>Created Date: {blog.createdAt}</p>
        <img src={blog.blogImage} alt="blogImage" height="250px" />
      </div>
    </>
  );
};

export default BlogDetails;
