import { Outlet, Route, Routes } from "react-router-dom";
import Navbar from "../Navbar.jsx";
import AllBlogs from "../Blog/AllBlogs.jsx";
import CreateBlog from "../Blog/CreateBlog.jsx";
import UpdateBlog from "../Blog/UpdateBlog.jsx";
import BlogDetails from "../Blog/BlogDetails.jsx";

const Router = () => {
  return (
    <Routes>
      <Route
        path="/"
        element={
          <>
            <Navbar />
            <Outlet />
          </>
        }
      >
        <Route index element={<div>This is Landing Page</div>} />

        <Route path="blogs" element={<Outlet />}>
          <Route index element={<AllBlogs />} />
          <Route path="create" element={<CreateBlog />} />

          <Route
            path="update"
            element={
              <div>
                <Outlet></Outlet>
              </div>
            }
          >
            <Route index element={<></>}></Route>
            <Route path=":blogId" element={<UpdateBlog></UpdateBlog>}></Route>
          </Route>
          <Route path=":blogId" element={<BlogDetails></BlogDetails>}></Route>
        </Route>
      </Route>
    </Routes>
  );
};

export default Router;
