import { NavLink } from "react-router-dom";

const Navbar = () => {
  return (
    <>
      <NavLink
        to="/blogs"
        style={{ marginRight: "15px", textDecoration: "none" }}
      >
        Blogs
      </NavLink>
      <NavLink
        to="/blogs/create"
        style={{ marginRight: "15px", textDecoration: "none" }}
      >
        Create Blog
      </NavLink>
    </>
  );
};

export default Navbar;
