import { configureStore } from "@reduxjs/toolkit";
import { blogSlice } from "../features/blogSlice.js";

export const store = configureStore({
  reducer: {
    blog: blogSlice,

    // [productApi.reducerPath]: productApi.reducer,
  },
  //   middleware: (getDefaultMiddleware) =>
  //     getDefaultMiddleware().concat([productApi.middleware]),
});
